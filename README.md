# CIC-CLI

This is early stage development. Don't expect user-friendly docs for awhile.


## INSTALL

You must have the bancor repository with the correct version, with `.abi`-files from a truffle compile execution in `<bancor_repo>/solidity/build/`.

You can build the typescript to javascript with `npm run compile`. The js files are put in the folder `build`.


## USAGE

After build:

`node build/cmd_ls.js` lists all smart-token in the Bancor Network stored in the given registry.

`node build/cmd_ls.js --help` to see available options.

You can also run the typescript directly with `ts-node src/cmd_ls.ts`


## CAVEATS

None of these scripts are safe to use for accounts that simultaneously are submitting transactions in other processes. The nonce is gotten once, and persists throughout the session.


## DEPENDENCIES

* bancor repository tree at tag `v0.6.8`
* node 12.x
* typescript 3.9.x
