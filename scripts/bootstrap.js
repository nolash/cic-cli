// please build before running this
// prerequisite: Anchor token
const path = require('path');
const fs = require('fs');
const Web3 = require('web3');

const config = require('../build/config.js');
const session = require('../build/session.js');
const LocalSigner = require('../build/signer.js').LocalSigner;
const rlpSignTransactionEIP115 = require('../build/signer.js').rlpSignTransactionEIP115;
const toContractArgument = require('../build/common.js').toContractArgument;
const toRegistryName = require('../build/registry.js').toRegistryName;


// TODO: pass session with state
const currentConfig = new config.Config({});
const currentSession = new session.Session(currentConfig);

const etherTokenAddress = process.argv[2]
if (!Web3.utils.isAddress(etherTokenAddress, currentSession.chainId)) {
	console.error(etherTokenAddress + ' is not a valid address for this chain (' + currentSession.chainId + ')');
	process.exit(1);
}

const privateKeyHex = process.env.CIC_PRIVATE_KEY;
const privateKey = Web3.utils.hexToBytes(privateKeyHex);
const currentSigner = new LocalSigner(privateKey);


function singleAddressAsArg(address) {
	const oneWordHex = Web3.utils.toHex(32);
	const oneWordHexPad = Web3.utils.padLeft(oneWordHex, 64);
	const addressLengthHex = Web3.utils.toHex(20);
	const addressLengthHexPad = Web3.utils.padLeft(addressLengthHex, 64);
	const registryHexPad = Web3.utils.padRight(address, 64);
	return oneWordHexPad.slice(2) + addressLengthHexPad.slice(2) + registryHexPad.slice(2) 
}


// TODO: add gas budget to contract entries
const deployState = {
	signer: currentSigner,
	nonce: -1,
	contracts: {
		'ContractRegistry': false, 	// gas used: 665513
		'BancorNetwork': true, 		// gas used: 3145019
		'ConverterFactory': false, 	// gas used: 1742524
		'BancorFormula': false, 	// gas used: 3506711
		'ConversionPathFinder': true, 	// gas used: 1069892
		'ConverterRegistry': true, 	// gas used: 2858036
		'ConverterRegistryData': true, 	// gas used: 1208774
		'LiquidTokenConverterFactory': false,
	},
	register: [ // "BancorNetwork" gas used: 119537 
		'BancorNetwork',
		'ConverterFactory',
		'BancorFormula',
		'ConversionPathFinder',
		'ConverterRegistry',
		'ConverterRegistryData',
	],
	deployCursor: 0,
};

function cheatRegistry(state) {
	return new Promise((whohoo, doh) => {
		state.contracts['ContractRegistry'] = {
			contractAddress: '0x9DC8BD57B1773532bf567EC5C7b1E8dC3eEF6291',
		};
		state.deployCursor = 1;
		whohoo(state);
	});
}

function cheatDeploy(state) {
	return new Promise((whohoo, doh) => {
		state.contracts['BancorNetwork'] = {
			contractAddress: '0xf03A5800DF66d7D942d77dBcbAd6F3f961abc7cA',
		};
		whohoo(state);
	});
}

function prepare(state) {
	return new Promise((whohoo, doh) => {
		const addrBytes = state.signer.getAddress();
		const addrHexUnsafe = Web3.utils.toHex(addrBytes);
		const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
		currentSession.web3.eth.getTransactionCount(addrHex, 'pending').then((nonceLatest) => {
			state.nonce = nonceLatest;
			whohoo(state);
		});
	});
}

function getStartGasForContract(contractName) {
	return 4000000; 
}

function registerNetwork(state) {
	return new Promise((whohoo, doh) => {
		registerNext(state, whohoo);
	});
}

function registerNext(state, cb) {
	const contractToRegister = state.register.shift();
	if (contractToRegister == undefined) {
		cb(state);
		return;
	};
	const contractRegistryName = toRegistryName(contractToRegister);
	const contractNameArgHex = Web3.utils.asciiToHex(contractRegistryName).slice(2);
	const contractNameArgHexPad = Web3.utils.padRight(contractNameArgHex, 64);
	const addressArgPad = Web3.utils.padLeft(state.contracts[contractToRegister].contractAddress, 64);
	const argsHex = contractNameArgHexPad + addressArgPad.slice(2);
	console.debug('registerNext', contractToRegister, argsHex);
	transact(
		state,
		state.contracts['ContractRegistry'].contractAddress,
		1000000, // TODO: improve
		'662DE379', // registerAddress(bytes32,address)
		argsHex,
	).then((newState) => {
		registerNext(newState, cb);
	});
}

function registerConverterFactory(state, cb) { // gas used: 119609
	const converterHex = state.contracts['LiquidTokenConverterFactory'].contractAddress.slice(2)
	const argsHex = Web3.utils.padLeft(converterHex, 64);
	console.debug('registerConverterFactory', state.contracts['ConverterFactory'].contractAddress, argsHex);
	return transact(
		state,
		state.contracts['ConverterFactory'].contractAddress,
		200000, // TODO: improve
		'12B4C6C1', // regsterTypedConverterFactory
		argsHex,
	);
}

function initializeFormulaTables(state, cb) { // gas used: 4502947
	console.debug('initializeFormula', state.contracts['BancorFormula'].contractAddress);
	return transact(
		state,
		state.contracts['BancorFormula'].contractAddress,
		5000000, // TODO: improve
		'E1C7392A', // init()
		'',
	);
}

function deployRegistry(state) {
	console.log('deployregistry', state);
	return deploy(state, 'ContractRegistry', getStartGasForContract('ContractRegistry'));
}

function deployNetwork(state) {
	return new Promise((whohoo, doh) => {
		deployNext(state, whohoo);
	});
}

function deployNext(state, cb) {
	console.debug('deploynext', state.deployCursor);
	const k = Object.keys(state.contracts);
	const contractName = k[state.deployCursor];
	let args = undefined;
	if (state.contracts[contractName]) {
		//args = singleAddressAsArg(state.contracts['ContractRegistry'].contractAddress);
		args = Web3.utils.padLeft(state.contracts['ContractRegistry'].contractAddress.slice(2), 64);
	}
	deploy(state, contractName, getStartGasForContract(contractName), args).then((newState) => {
		if (newState.deployCursor == k.length) {
			cb(newState);
			return;
		}
		deployNext(newState, cb);
	});
}


// TODO: make re-entrant so we can deploy all contracts in parallell
function deploy(state, name, startGas, argsHex) {
	const nonce = state.nonce; //currentSession.useNonce();

	// block for debug only	
	const addrBytes = state.signer.getAddress();
	const addrHexUnsafe = Web3.utils.toHex(addrBytes);
	const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
	console.debug('deploy ', name, nonce, startGas, argsHex, addrHex);

	// load the registry byte code 
	const bundleFile = path.join(
		currentSession.paths['bancor_build'],
		'contracts',
		name + '.json',
	);

	const bundleString = fs.readFileSync(
		bundleFile,
		{
			encoding: 'utf-8',
			flag: 'r',
		}
	);

	const bundle = JSON.parse(bundleString);
	let byteCodeHex = Web3.utils.toHex(bundle.bytecode);

	if (argsHex != undefined) {
		byteCodeHex += argsHex;	
	}

	const deployCode = Web3.utils.hexToBytes(byteCodeHex);

	return new Promise(async (whohoo, doh) => {
		const gasPrice = await currentSession.web3.eth.getGasPrice();
		const txUnsigned = [
			nonce,
			parseInt(gasPrice, 10),
			startGas,
			new Uint8Array(0),
			0,
			new Uint8Array(deployCode),
			currentSession.chainId,
			0, 
			0,
		];
		const txSigned = rlpSignTransactionEIP115(txUnsigned, state.signer);
		const txSignedHex = Web3.utils.toHex(txSigned);
		//console.debug('deploy signedhex', txSignedHex);
		try {
			const r = await currentSession.web3.eth.sendSignedTransaction(txSignedHex);
			console.debug('deploy receipt', name, r.contractAddress, r.cumulativeGasUsed);
			state.nonce++;
			state.contracts[name] = r;
			state.deployCursor++;
			whohoo(state);
		}
		catch(e) {
			doh('tx failed:' + e);
		}
	});
}

function transact(state, contractAddressHex, startGas, methodHex, argsHex) {
	const nonce = state.nonce;

	// block for debug only	
	const addrBytes = state.signer.getAddress();
	const addrHexUnsafe = Web3.utils.toHex(addrBytes);
	const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
	console.debug('transact ', contractAddressHex, methodHex, nonce, startGas, argsHex, addrHex);

	const transactCode = Web3.utils.hexToBytes('0x' + methodHex + argsHex);
	const contractAddress = Web3.utils.hexToBytes(contractAddressHex);

	return new Promise(async (whohoo, doh) => {
		const gasPrice = await currentSession.web3.eth.getGasPrice();
		const txUnsigned = [
			nonce,
			parseInt(gasPrice, 10),
			startGas,
			new Uint8Array(contractAddress),
			0,
			new Uint8Array(transactCode),
			currentSession.chainId,
			0, 
			0,
		];
		console.debug('tx unsigned', txUnsigned);
		const txSigned = rlpSignTransactionEIP115(txUnsigned, state.signer);
		const txSignedHex = Web3.utils.toHex(txSigned);
		console.debug('transact signedhex', txSignedHex);
		try {
			const r = await currentSession.web3.eth.sendSignedTransaction(txSignedHex);
			console.debug('transact receipt', contractAddressHex, r);
			state.nonce++;
			whohoo(state);
		}
		catch(e) {
			const rlp = require('rlp');
			console.error('error on rlp', rlp.decode(txSigned));
			doh('tx failed:' + e);
		}
	});
}

prepare(deployState)
//.then(cheatRegistry)
.then(deployRegistry)
//.then(cheatDeploy)
.then(deployNetwork)
.then(registerNetwork)
.then(registerConverterFactory)
.then(initializeFormulaTables)
.then((o) => {
	console.log('eeybdeeybdeeybdeeyb that\'s all folks', o);
	process.exit(0);
}).catch((e) => {
	console.error('FAIL!!', e)
	process.exit(1);
});
