const yargs = require('yargs');

const standardOptions = yargs.option('wallet-provider', {
	alias: 'w',
	type: 'string',
	description: 'set wallet provider (default: value of --provider)',
}).option('provider', {
	alias: 'p',
	type: 'string',
	description: 'set wallet provider (default: ws://localhost:8546)',
}).option('bancor-dir', {
	type: 'string',
	description: 'bancor dir path (default contrib/bancor)',
}).option('registry-address', {
	type: 'string',
	description: 'registry contract address',
});

export { standardOptions };
