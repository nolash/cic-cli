const Web3 = require('web3');


import { Config } from './config';
import { Session } from './session';
import { standardOptions } from './args';

let argv = standardOptions.argv;

let currentConfig = new Config({
	provider: argv['provider'],
	walletProvider: argv['wallet-provider'],
	bancorDir: argv['bancor-dir'],
	registryAddress: argv['registry-address'],
});

console.debug('config', currentConfig);

let currentSession = new Session(currentConfig);

console.debug('blockchain provider', currentSession.web3.eth.currentProvider.url);
console.debug('wallet provider', currentSession.web3.eth.personal.currentProvider.url);

currentSession.web3.eth.getBlockNumber().then((r) => {
	console.debug('blocknumber', r);
});

currentSession.web3w.eth.personal.getAccounts().then((r) => {
	console.debug('accounts', r);
});

let bancorNetworkIdString = Web3.utils.padRight(Web3.utils.asciiToHex(argv._[0]), 64);
currentSession.contracts['ContractRegistry'].methods.addressOf(bancorNetworkIdString).call().then((o) => {
	console.debug('methods', argv._[0], bancorNetworkIdString, o);
	process.exit(0);
});
