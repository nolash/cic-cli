const Web3 = require('web3');
const path = require('path');
const fs = require('fs');

import { Config } from './config';
import { Session } from './session';
import { standardOptions } from './args';
import { toContractArgument } from './common';
import { rlpSignTransactionEIP115, LocalSigner } from './signer';
import { reserveAddress } from './network';

let argv = standardOptions.option('token-name', {
	type: 'string',
	description: 'name of token',
}).option('token-symbol', {
	type: 'string',
	description: 'name of token',
}).demandOption(
	['token-symbol',
	'token-name'],
).argv;
	

let currentConfig = new Config({
	provider: argv['provider'],
	walletProvider: argv['wallet-provider'],
	bancorDir: argv['bancor-dir'],
	registryAddress: argv['registry-address'],
});

let currentSession = new Session(currentConfig);

const bundleFile = path.join(
	currentSession.paths['bancor_build'],
	'contracts',
	'SmartToken.json',
);
const bundleString = fs.readFileSync(
	bundleFile,
	{
		encoding: 'utf-8',
		flag: 'r',
	}
);

// argument example:
// 0000000000000000000000000000000000000000000000000000000000000001
// 00000000000000000000000000000000000000000000000000000000000000e0
// 0000000000000000000000000000000000000000000000000000000000000120
// 0000000000000000000000000000000000000000000000000000000000000012
// 000000000000000000000000000000000000000000000000000000000007a120
// 0000000000000000000000000000000000000000000000000000000000000160
// 00000000000000000000000000000000000000000000000000000000000001c0
// 000000000000000000000000000000000000000000000000000000000000001a
// 45584f4c4f56455220536d6172742052656c617920546f6b656e000000000000
// 0000000000000000000000000000000000000000000000000000000000000006
// 45584f424e540000000000000000000000000000000000000000000000000000
// 0000000000000000000000000000000000000000000000000000000000000001
// 0000000000000000000000001f573d6fb3f13d689ff844b4ce37794d79a7ff1c
// 0000000000000000000000000000000000000000000000000000000000000001
// 000000000000000000000000000000000000000000000000000000000007a120

// Interfaces bancor 0.6.8 
// For now we are forcing users to use type 0 converter,

const converterTypePad = toContractArgument(0);
const decimalsPad = toContractArgument(18);
const maxConversionFeePad = toContractArgument(100000);

const dynamicIndexName = toContractArgument(7 * 32);
const dynamicIndexSymbol = toContractArgument(9 * 32);
const tokenName = toContractArgument(argv['token-name']);
const tokenSymbol = toContractArgument(argv['token-symbol']);

const dynamicIndexReserveTokens = toContractArgument(11 * 32);
const dynamicIndexReserveWeights = toContractArgument(13 * 32);
const reserveTokenAddress = toContractArgument(Web3.utils.hexToBytes(reserveAddress))
const reserveTokenWeightPad = toContractArgument(250000);
const dynamicArrayLength = toContractArgument(1);

const methodHex = '5a0a6618';

const argsHex = 
	methodHex +
	converterTypePad +
	dynamicIndexName +
	dynamicIndexSymbol +
	decimalsPad +
	maxConversionFeePad + 
	dynamicIndexReserveTokens +
	dynamicIndexReserveWeights +
	tokenName +
	tokenSymbol +
	dynamicArrayLength +
	reserveTokenAddress +
	dynamicArrayLength +
	reserveTokenWeightPad

console.debug(argsHex);


// TODO: use rpc instead for singing
currentSession.web3w.eth.personal.getAccounts().then(async (a) => {

	const converterRegistryContract = await currentSession.loadContract('ConverterRegistry');
	console.log(converterRegistryContract);
	const converterRegistryContractBytes = Web3.utils.hexToBytes(converterRegistryContract._address);

	const privateKeyHex = process.env.CIC_PRIVATE_KEY;
	const privateKey = Web3.utils.hexToBytes(privateKeyHex);
	const signer = new LocalSigner(privateKey);
	const addrBytes = signer.getAddress();
	const addrHexUnsafe = Web3.utils.toHex(addrBytes);
	const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
	const nonceLatest = await currentSession.web3.eth.getTransactionCount(addrHex, 'latest');
	//const noncePending = await currentSession.web3.eth.getTransactionCount(addrHex, 'pending');
	const gasPrice = await currentSession.web3.eth.getGasPrice();
	console.debug('address', addrHex);
	console.debug('nonce', nonceLatest);
	console.debug('gasprice', gasPrice);

	const startGas = 4500000;

	//const deployCode = Web3.utils.hexToBytes(byteCodeHex + contractArgs);
	const argsCode = Web3.utils.hexToBytes('0x' + argsHex);
	const txUnsigned = [
		nonceLatest,
		parseInt(gasPrice, 10),
		startGas,
		new Uint8Array(converterRegistryContractBytes),
		0,
		new Uint8Array(argsCode),
		currentSession.chainId,
		0, 
		0,
	];
	const txSigned = rlpSignTransactionEIP115(txUnsigned, signer);
	const txSignedHex = Web3.utils.toHex(txSigned);
	console.debug('signedhex', txSignedHex);
	const r = await currentSession.web3.eth.sendSignedTransaction(txSignedHex);
	console.log(r);
	process.exit(0);
});
