const Web3 = require('web3');
const path = require('path');
const fs = require('fs');

import { Config } from './config';
import { Session } from './session';
import { standardOptions } from './args';
import { toContractArgument } from './common';
import { rlpSignTransactionEIP115, LocalSigner } from './signer';

let argv = standardOptions.option('token-name', {
	type: 'string',
	description: 'name of token',
}).option('token-symbol', {
	type: 'string',
	description: 'name of token',
}).demandOption(
	['token-symbol',
	'token-name'],
).argv;
	

let currentConfig = new Config({
	provider: argv['provider'],
	walletProvider: argv['wallet-provider'],
	bancorDir: argv['bancor-dir'],
	registryAddress: argv['registry-address'],
});

let currentSession = new Session(currentConfig);

const bundleFile = path.join(
	currentSession.paths['bancor_build'],
	'contracts',
	'SmartToken.json',
);
const bundleString = fs.readFileSync(
	bundleFile,
	{
		encoding: 'utf-8',
		flag: 'r',
	}
);
const bundle = JSON.parse(bundleString);
const byteCodeHex = Web3.utils.toHex(bundle.bytecode);
const abi = bundle.abi;

// arg strings for SmartToken are at
// 3rd and 5th evm word position
const dynamicArgIndices = toContractArgument(96) + toContractArgument(160);

const tokenNameArg = toContractArgument(argv['token-name']);
const tokenSymbolArg = toContractArgument(argv['token-symbol']);
const decimalsArg = toContractArgument(18);
console.debug('token', '"' + argv['token-name'] + '"', argv['token-symbol'], 18);

const contractArgs = dynamicArgIndices + decimalsArg + tokenNameArg + tokenSymbolArg;
console.debug('contract args', contractArgs);

// TODO: use rpc instead
currentSession.web3w.eth.personal.getAccounts().then(async (a) => {
	const privateKeyHex = process.env.CIC_PRIVATE_KEY;
	const privateKey = Web3.utils.hexToBytes(privateKeyHex);
	const signer = new LocalSigner(privateKey);
	const addrBytes = signer.getAddress();
	const addrHexUnsafe = Web3.utils.toHex(addrBytes);
	const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
	const nonceLatest = await currentSession.web3.eth.getTransactionCount(addrHex, 'latest');
	//const noncePending = await currentSession.web3.eth.getTransactionCount(addrHex, 'pending');
	const gasPrice = await currentSession.web3.eth.getGasPrice();
	console.debug('address', addrHex);
	console.debug('nonce', nonceLatest);
	console.debug('gasprice', gasPrice);

	const startGas = 1200000;

	const deployCode = Web3.utils.hexToBytes(byteCodeHex + contractArgs);
	const txUnsigned = [
		nonceLatest,
		parseInt(gasPrice, 10),
		startGas,
		new Uint8Array(0),
		0,
		new Uint8Array(deployCode),
		currentSession.chainId,
		0, 
		0,
	];
	const txSigned = rlpSignTransactionEIP115(txUnsigned, signer);
	const txSignedHex = Web3.utils.toHex(txSigned);
	console.debug('signedhex', txSignedHex);
	const r = await currentSession.web3.eth.sendSignedTransaction(txSignedHex);
	console.log(r);
	process.exit(0);
});
