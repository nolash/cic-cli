const Web3 = require('web3');

import { Config } from './config';
import { Session } from './session';
import { standardOptions } from './args';

let argv = standardOptions.argv;

let currentConfig = new Config({
	provider: argv['provider'],
	walletProvider: argv['wallet-provider'],
	bancorDir: argv['bancor-dir'],
	registryAddress: argv['registry-address'],
});

let currentSession = new Session(currentConfig);

currentSession.loadContract('ConverterRegistry').then(async (o) => {
	const tokenAddresses = await currentSession.contracts['ConverterRegistry'].methods.getAnchors().call();
	for (let i = 0; i < tokenAddresses.length; i++) {
		await currentSession.loadToken(tokenAddresses[i]);
	}
	console.log(Object.keys(currentSession.tokens));
	console.log(await currentSession.contracts['ConverterRegistry'].methods.getConvertersByAnchors([tokenAddresses[0]]).call());
	Object.keys(currentSession.tokens).map(t => console.debug('token', t, currentSession.tokens[t]));
	process.exit(0);
}).catch((e) => {
	console.error('err', e);
	process.exit(1);
});
