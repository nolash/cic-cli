const Web3 = require('web3');
const keccak = require('keccak');

function toContractId(name: string):string {
	if (name.length == 0) {
		throw 'empty string';
	}
	if (name.length > 32) {
		throw 'string must be max 32 characters';
	}
	const hex = Web3.utils.asciiToHex(name);
	return Web3.utils.padRight(hex, 32);
}

function toContractArgument(arg: any):string {
	if (typeof arg == 'string') {
		if (arg.length > 32) {
			throw 'cannot handle string length > 32'
		}
		const argLengthHex = Web3.utils.toHex(arg.length);
		const argLengthHexPad = Web3.utils.padLeft(argLengthHex, 64);
		const argContentHex = Web3.utils.toHex(arg);
		const argContentHexPad = Web3.utils.padRight(argContentHex, 64);
		return argLengthHexPad.substring(2) + argContentHexPad.substring(2);
	} else if (typeof arg == 'number') {
		const numberHex = Web3.utils.toHex(arg);
		const numberHexPad = Web3.utils.padLeft(numberHex, 64);
		return numberHexPad.substring(2);
	} else if (typeof arg == 'object' && arg.length != undefined) { // array-ish
		if (arg.length > 32) {
			throw 'cannot handle bytes length > 32'
		}
		const bytesHex = Web3.utils.bytesToHex(arg).slice(2);
		const bytesHexPad = Web3.utils.padLeft(bytesHex, 64);
		return bytesHexPad;
	} else {
		throw 'invalid argument type ' + typeof arg + ' or not implemented';
	}
}

function publicKeyToAddress(publicKey: Uint8Array):Uint8Array {
        // convert public key to buffer data
        let pubKeyNoPrefixArray = publicKey.slice(1);
        let pubKeyNoPrefixBuffer = Buffer.from(pubKeyNoPrefixArray);

        // hash the public key and slice the result to obtain the address
        let hasher = keccak('keccak256');
        hasher.update(pubKeyNoPrefixBuffer);
        let addressBaseBuffer = hasher.digest();
        let addressArray = Array.prototype.slice.call(addressBaseBuffer, 12);
        let addressBuffer = Buffer.from(addressArray);
        return addressBuffer;
}

export {
	toContractId,
	toContractArgument,
	publicKeyToAddress,
};
