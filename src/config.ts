let web3 = require('web3');

function validProviderString(s:string):boolean {
	console.debug('provider string validator not implemented');
	return true;
}

class Config {
	
	provider;
	walletProvider;
	bancorDir: string;
	registryAddress;

	constructor(config: any) {
		let providerString = 'ws://localhost:8546';
		if (config.provider != undefined) {
			if (!validProviderString(config.provider)) {
				throw 'invalid provider string ' + config.provider;
			}
			providerString = config.provider;
		}

		let walletProviderString = providerString;
		if (config.walletProvider != undefined) {
			if (!validProviderString(config.walletProvider)) {
				throw 'invalid wallet provider string ' + config.walletProvider;
			}
			walletProviderString = config.walletProvider
		}

		let bancorDirString = './contrib/bancor';
		if (config.bancorDir != undefined) {
			bancorDirString = config.bancorDir;
		}

		if (config.registryAddress != undefined) {
			this.registryAddress = web3.utils.toChecksumAddress(config.registryAddress);
		}

		this.provider = providerString;
		this.walletProvider = walletProviderString;
		this.bancorDir = bancorDirString;
	}
}

export { Config };
