const chainId = 8995;
const networkName = 'bloxberg';
const reserveAddress = '0xa3e041874b1f6dD4aC53705b48F0328c75D3B4f3';

export {
	chainId,
	networkName,
	reserveAddress,
};
