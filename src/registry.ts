// values are taken from ContractRegistryClient.sol contract in bancor suite
const contractName = [
	"ContractRegistry",
	"BancorNetwork",
	"BancorFormula",
	"ConverterFactory",
	"ConversionPathFinder",
	"ConverterRegistry",
	"ConverterRegistryData",
]

const contractRegistryName = {
	"ContractRegistry": "ContractRegistry",
	"BancorNetwork": "BancorNetwork",
	"BancorFormula": "BancorFormula",
	"ConverterFactory": "ConverterFactory",
	"ConversionPathFinder": "ConversionPathFinder",
	"ConverterRegistry": "BancorConverterRegistry",
	"ConverterRegistryData": "BancorConverterRegistryData",
}

function toRegistryName(s: string):string {
	if (contractRegistryName[s] == undefined) {
		throw 'unknown registry entry name "' + s + '"';
	}
	return contractRegistryName[s];
}

export {
	toRegistryName,
}
