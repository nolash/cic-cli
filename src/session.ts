const Web3 = require('web3');
const path = require('path');
const fs = require('fs');
const net = require('net');
import { Config } from './config';
import { toContractId } from './common';
import { chainId, networkName } from './network';
import { Signer } from './signer';
import { toRegistryName } from './registry';

// TODO: perform bancor dep version check
class Session {
	web3;
	web3w;
	registryContract;
	contracts = {};
	paths = {};
	tokens = {};
	converters = {};
	chainId;
	network: string;
	nonce: number;
	cursor: number;
	signer: Signer;

	constructor(config: Config) {
		const ethProvider = new Web3.providers.WebsocketProvider(config.provider);
		this.web3 = new Web3(ethProvider);

		const personalProvider = new Web3.providers.WebsocketProvider(config.walletProvider);
		//const personalProvider = new Web3.providers.IpcProvider(config.walletProvider, net);
		this.web3w = new Web3(personalProvider);

		this.paths['bancor_build'] = path.join(
			config.bancorDir,
			'solidity/build',
		);
		if (config.registryAddress != undefined) {
			this.setRegistry(config.registryAddress);
		}
		this.network = networkName;
		this.chainId = chainId;
		this.cursor = 0;
	}

	public async setRegistry(address: string) {
		this.loadContract('ContractRegistry', address);
		this.registryContract = address; //this.contracts['ContractRegistry'];
	}

	public async loadContract(name: string, address?: string):Promise<any> {
		const abi = this.getAbi(name);	

		let cAddress = '';
		if (address == undefined) {
			const registryContractName = toRegistryName(name);
			const contractId = toContractId(registryContractName);
			cAddress = await this.contracts['ContractRegistry'].methods.addressOf(contractId).call();
			console.debug('looked up contract address', name, cAddress);
		} else {
			cAddress = Web3.utils.toChecksumAddress(address);
			console.debug('setting contract address', name, cAddress);
		}

		// TODO: either find address accessor in contract object or create one here
		this.contracts[name] = new this.web3.eth.Contract(abi, cAddress);
		return this.contracts[name];
	}

	public async loadToken(address: string) {
		const abi = this.getAbi('SmartToken');

		const cAddress = await Web3.utils.toChecksumAddress(address);
		const contract = new this.web3.eth.Contract(abi, cAddress);
		const tokenName = await contract.methods.name().call();
		const tokenSymbol = await contract.methods.symbol().call();
		const tokenConverter = await contract.methods.owner().call();
		const tokenSupply = await contract.methods.totalSupply().call();
		const tokenOwner = await contract.methods.owner().call();

		this.tokens[tokenSymbol] = {
			'name': tokenName,
			'address': cAddress,
			'supply': tokenSupply,
			'owner': tokenOwner,
		}
		console.debug('token added', tokenSymbol, tokenName, cAddress);
		
	}

	public getAbi(name: string) {
		const bundleFile = path.join(
			this.paths['bancor_build'],
			'contracts',
			name + '.json',
		);
		const bundleString = fs.readFileSync(
			bundleFile,
			{
				encoding: 'utf-8',
				flag: 'r',
			}
		);
		const bundle = JSON.parse(bundleString);
		return bundle.abi;
	}
}

export { Session };
