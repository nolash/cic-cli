const Web3 = require('web3');
const keccak = require('keccak');
const secp256k1 = require('secp256k1');
const rlp = require('rlp');

import { publicKeyToAddress } from './common';

// remember, msg signing is: "\x19Ethereum Signed Message:\n" + string(len(msg)) + msg
// we don't use that here yet, though.
const ethereumCorporateSignPrefix  = Web3.utils.hexToBytes('0x19457468657265756d205369676e6564204d6573736167653a0a');


interface Signature {
	signature: Buffer,
	recid: number,
}


interface Signer {
	sign(digest: Uint8Array):Signature;
}


class LocalSigner {
	privateKey: Uint8Array;
	publicKey: Uint8Array;
	address: Uint8Array;

	constructor(privateKey: Uint8Array) {
		console.warn('LocalSigner is for testing purposes only, it\'s absolutely unsafe');
		this.privateKey = Buffer.from(privateKey);
		this.publicKey = secp256k1.publicKeyCreate(this.privateKey, false);
        	this.address = publicKeyToAddress(this.publicKey);
	}

	public sign(digest: Uint8Array):Signature {
		return secp256k1.ecdsaSign(Buffer.from(digest), this.privateKey);
	}

	public getAddress():Uint8Array {
		return this.address;
	}
}


function rlpSignTransactionEIP115(tx: Array<any>, signer: Signer):Uint8Array {
	// encode and sum
	const e = rlp.encode(tx);
	const h = keccak('keccak256');
	h.update(e);
	const g = h.digest();

	// sign
	const z = signer.sign(g);

	// insert signature in placeholder fields
	tx[6] = (tx[6] * 2) + 35 + z.recid;
	tx[7] = z.signature.slice(0, 32);
	tx[8] = z.signature.slice(32);

	return rlp.encode(tx);
}


export {
	rlpSignTransactionEIP115,
	LocalSigner,
	Signer,
}
