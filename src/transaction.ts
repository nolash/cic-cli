const Web3 = require('web3');
const fs = require('fs');
const path = require('path');

import { Session } from './session';


// TODO: make re-entrant so we can deploy all contracts in parallell
function deploy(session: Session, name: string, startGas: number, argsHex: string):Promise<Session> {
	const nonce = state.nonce; //currentSession.useNonce();

	// block for debug only	
	const addrBytes = state.signer.getAddress();
	const addrHexUnsafe = Web3.utils.toHex(addrBytes);
	const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
	console.debug('deploy ', name, nonce, startGas, argsHex, addrHex);

	// load the registry byte code 
	const bundleFile = path.join(
		currentSession.paths['bancor_build'],
		'contracts',
		name + '.json',
	);

	const bundleString = fs.readFileSync(
		bundleFile,
		{
			encoding: 'utf-8',
			flag: 'r',
		}
	);

	const bundle = JSON.parse(bundleString);
	let byteCodeHex = Web3.utils.toHex(bundle.bytecode);

	if (argsHex != undefined) {
		byteCodeHex += argsHex;	
	}

	const deployCode = Web3.utils.hexToBytes(byteCodeHex);

	return new Promise(async (whohoo, doh) => {
		const gasPrice = await currentSession.web3.eth.getGasPrice();
		const txUnsigned = [
			nonce,
			parseInt(gasPrice, 10),
			startGas,
			new Uint8Array(0),
			0,
			new Uint8Array(deployCode),
			currentSession.chainId,
			0, 
			0,
		];
		const txSigned = rlpSignTransactionEIP115(txUnsigned, session.signer);
		const txSignedHex = Web3.utils.toHex(txSigned);
		//console.debug('deploy signedhex', txSignedHex);
		try {
			const r = await currentSession.web3.eth.sendSignedTransaction(txSignedHex);
			console.debug('deploy receipt', name, r.contractAddress, r.cumulativeGasUsed);
			session.nonce++;
			session.contracts[name] = r;
			session.cursor++;
			whohoo(state);
		}
		catch(e) {
			doh('tx failed:' + e);
		}
	});
}

function transact(session: Session, contractAddressHex: string, startGas: number, methodHex string, argsHex string):Promise<Session> {
	const nonce = state.nonce;

	// block for debug only	
	const addrBytes = state.signer.getAddress();
	const addrHexUnsafe = Web3.utils.toHex(addrBytes);
	const addrHex = Web3.utils.toChecksumAddress(addrHexUnsafe);
	console.debug('transact ', contractAddressHex, methodHex, nonce, startGas, argsHex, addrHex);

	const transactCode = Web3.utils.hexToBytes('0x' + methodHex + argsHex);
	const contractAddress = Web3.utils.hexToBytes(contractAddressHex);

	return new Promise(async (whohoo, doh) => {
		const gasPrice = await currentSession.web3.eth.getGasPrice();
		const txUnsigned = [
			nonce,
			parseInt(gasPrice, 10),
			startGas,
			new Uint8Array(contractAddress),
			0,
			new Uint8Array(transactCode),
			currentSession.chainId,
			0, 
			0,
		];
		console.debug('tx unsigned', txUnsigned);
		const txSigned = rlpSignTransactionEIP115(txUnsigned, session.signer);
		const txSignedHex = Web3.utils.toHex(txSigned);
		console.debug('transact signedhex', txSignedHex);
		try {
			const r = await currentSession.web3.eth.sendSignedTransaction(txSignedHex);
			console.debug('transact receipt', contractAddressHex, r);
			session.nonce++;
			whohoo(state);
		}
		catch(e) {
			const rlp = require('rlp');
			console.error('error on rlp', rlp.decode(txSigned));
			doh('tx failed:' + e);
		}
	});
}

export {
	deploy,
	transact,
}
