const assert = require('assert');
const common = require('../src/common');


//32 bytes:'0000000000000000000000000000000000000000000000000000000000000000'
describe('common_contract', () => {

	it('toContractArgumentString', () => {
		const arg = 'foo bar';
		const argEncoded = common.toContractArgument(arg);
		assert.equal(argEncoded, '0000000000000000000000000000000000000000000000000000000000000007666f6f2062617200000000000000000000000000000000000000000000000000');
					    
	});

	it('toContractArgumentNumber', () => {
		const arg = 42;
		const argEncoded = common.toContractArgument(arg);
		assert.equal(argEncoded, '000000000000000000000000000000000000000000000000000000000000002a');
					    
	});
});
